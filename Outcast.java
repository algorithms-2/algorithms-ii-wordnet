public class Outcast {
    private WordNet wordNet;

    public Outcast(WordNet wordnet) {
        this.wordNet = wordnet;
    }

    public String outcast(String[] nouns) {
        int maxDist = Integer.MIN_VALUE;
        String outcast = null;
        for (int i = 0; i < nouns.length; i++) {
            String noun = nouns[i];
            int nounDist = 0;
            for (int j = 0; j < nouns.length; j++) {
                nounDist += this.wordNet.distance(noun, nouns[j]);
            }
            if (nounDist > maxDist) {
                maxDist = nounDist;
                outcast = noun;
            }
        }
        return outcast;
    }
}
