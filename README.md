# Algorithms II Assignment 1: WordNet
Solution for assignment 1 of the Princeton University Algorithms II course on Coursera (https://coursera.cs.princeton.edu/algs4/assignments/wordnet/specification.php).

The goal of this assignment was to provide an efficient way to traverse the WordNet overview of English nouns in the form of a directed graph. WordNet.java is the central class, which provides methods for computing the semantic distance between two nouns or sets of nouns in WordNet, as well as finding their nearest common ancestor. This is done with the help of SAP.java, which is a more general-purpose digraph processing tool. Finally, Outcast.java is an example of a client using the WordNet class to find out which word in a list of words is semantically least related to the others, according to its classification in WordNet.

## Results  

Correctness:  36/36 tests passed

Memory:       4/4 tests passed

Timing:       31/27 tests passed

<b>Aggregate score:</b> 102.96%*
*includes points for bonus timing tests.


## How to use
The classes can be used independently or together as described above. Intended to be used with a .txt file of the synsets and hypernyms in the WordNet archive, which is provided for this assignment. See the above assignment description for information on valid input file formats.
<b>NOTE:</b> Depends on the algs4 library used in the course.
