import edu.princeton.cs.algs4.Digraph;
import edu.princeton.cs.algs4.DirectedCycle;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.SET;

import java.util.HashMap;

public class WordNet {

    private final int V;
    private final HashMap<String, SET<Integer>> nounToIds;
    private final HashMap<Integer, String> idToSynset;
    private final Digraph digraph;
    private final SAP sap;

    public WordNet(String synsets, String hypernyms) {
        if (synsets == null || hypernyms == null) throw new IllegalArgumentException();
        In inSynsets = new In(synsets);
        In inHypernyms = new In(hypernyms);

        String[] synsetLines = inSynsets.readAll().split("\n");
        this.V = synsetLines.length;
        this.nounToIds = new HashMap<>();
        this.idToSynset = new HashMap<>();
        loadSynsets(synsetLines);

        this.digraph = new Digraph(V);
        loadHypernyms(inHypernyms.readAll().split("\n"));
        if (graphIsNotDAG()) throw new IllegalArgumentException();
        this.sap = new SAP(this.digraph);
    }

    private boolean graphIsNotDAG() {
        int rootCount = 0;
        for (int i = 0; i < this.V; i++) {
            if (!this.digraph.adj(i).iterator().hasNext()) rootCount++;
            if (rootCount > 1) break;
        }
        if (rootCount != 1) return true;
        DirectedCycle diCycle = new DirectedCycle(this.digraph);
        return diCycle.hasCycle();
    }

    private void loadSynsets(String[] lines) {
        for (int i = 0; i < lines.length; i++) {
            String[] commaSeparated = lines[i].split(",");
            int id = Integer.parseInt(commaSeparated[0]);
            String synset = commaSeparated[1];
            this.idToSynset.put(id, synset);
            loadSynsetNouns(id, synset.split(" "));
        }
    }

    private void loadSynsetNouns(int id, String[] nouns) {
        for (int j = 0; j < nouns.length; j++) {
            String noun = nouns[j];
            if (!this.nounToIds.containsKey(noun)) {
                this.nounToIds.put(noun, new SET<>());
            }
            this.nounToIds.get(noun).add(id);
        }
    }

    private void loadHypernyms(String[] lines) {
        for (int i = 0; i < lines.length; i++) {
            String[] line = lines[i].split(",");
            for (int j = 1; j < line.length; j++) {
                int firstId = Integer.parseInt(line[0]);
                this.digraph.addEdge(firstId, Integer.parseInt(line[j]));
            }
        }
    }

    public Iterable<String> nouns() {
        return this.nounToIds.keySet();
    }

    public boolean isNoun(String word) {
        if (word == null) throw new IllegalArgumentException();
        return this.nounToIds.containsKey(word);
    }

    public int distance(String nounA, String nounB) {
        if (!isNoun(nounA) || !isNoun(nounB)) throw new IllegalArgumentException();
        return this.sap.length(this.nounToIds.get(nounA), this.nounToIds.get(nounB));
    }

    public String sap(String nounA, String nounB) {
        if (!isNoun(nounA) || !isNoun(nounB)) throw new IllegalArgumentException();
        int ancestorId = this.sap.ancestor(this.nounToIds.get(nounA), this.nounToIds.get(nounB));
        return ancestorId > -1 ? this.idToSynset.get(ancestorId) : null;
    }
}
