import edu.princeton.cs.algs4.Digraph;
import edu.princeton.cs.algs4.Queue;

import java.util.HashMap;
import java.util.Map;

public class SAP {
    private static final int INFINITY = Integer.MAX_VALUE;
    private static final String LENGTH = "return_length";
    private static final String ANCESTOR = "return_ancestor";
    private final Digraph digraph;
    private final int V;

    public SAP(Digraph G) {
        if (G == null) throw new IllegalArgumentException();
        this.digraph = new Digraph(G);
        this.V = this.digraph.V();
    }

    public int length(int v, int w) {
        if (!validVertex(v) || !validVertex(w)) throw new IllegalArgumentException();
        return processDistances(bfs(this.digraph, v), bfs(this.digraph, w), LENGTH);
    }

    public int length(Iterable<Integer> v, Iterable<Integer> w) {
        if (!validVertices(v) || !validVertices(w)) throw new IllegalArgumentException();
        if (!v.iterator().hasNext() || !w.iterator().hasNext()) return -1;
        return processDistances(bfs(this.digraph, v), bfs(this.digraph, w), LENGTH);
    }

    public int ancestor(int v, int w) {
        if (!validVertex(v) || !(validVertex(w))) throw new IllegalArgumentException();
        return processDistances(bfs(this.digraph, v), bfs(this.digraph, w), ANCESTOR);
    }

    public int ancestor(Iterable<Integer> v, Iterable<Integer> w) {
        if (!validVertices(v) || !validVertices(w)) throw new IllegalArgumentException();
        if (!v.iterator().hasNext() || !w.iterator().hasNext()) return -1;
        return processDistances(bfs(this.digraph, v), bfs(this.digraph, w), ANCESTOR);
    }

    private HashMap<Integer, Integer> bfs(Digraph G, int s) {
        HashMap<Integer, Boolean> visited = new HashMap<>();
        HashMap<Integer, Integer> distToSrc = new HashMap<>();
        Queue<Integer> q = new Queue<>();
        distToSrc.put(s, 0);
        visited.put(s, true);
        q.enqueue(s);
        return processBfsQueue(q, G, visited, distToSrc);
    }

    private HashMap<Integer, Integer> bfs(Digraph G, Iterable<Integer> sources) {
        HashMap<Integer, Boolean> visited = new HashMap<>();
        HashMap<Integer, Integer> distToSrc = new HashMap<>();
        Queue<Integer> q = new Queue<>();
        for (int s : sources
        ) {
            distToSrc.put(s, 0);
            visited.put(s, true);
            q.enqueue(s);
        }
        return processBfsQueue(q, G, visited, distToSrc);
    }

    private HashMap<Integer, Integer> processBfsQueue(Queue<Integer> q, Digraph G,
                                                      HashMap<Integer, Boolean> visited,
                                                      HashMap<Integer, Integer> distToSrc) {
        while (!q.isEmpty()) {
            int v = q.dequeue();
            for (int w : G.adj(v)) {
                if (visited.get(w) == null) {
                    q.enqueue(w);
                    distToSrc.put(w, distToSrc.get(v) + 1);
                    visited.put(w, true);
                }
            }
        }
        return distToSrc;
    }

    private int processDistances(HashMap<Integer, Integer> distsV, HashMap<Integer, Integer> distsW,
                                 String returns) {
        int shortest = INFINITY;
        int ancestor = -1;
        for (Map.Entry<Integer, Integer> e : distsV.entrySet()
        ) {
            int key = e.getKey();
            int distToV = e.getValue();
            Integer distToW = distsW.get(key);
            if (distToW == null) continue;
            if (distToV + distsW.get(key) < shortest) {
                shortest = distToV + distToW;
                ancestor = key;
            }
        }
        if (returns.equals(ANCESTOR)) return ancestor;
        return shortest < INFINITY ? shortest : -1;
    }

    private boolean validVertex(int v) {
        return v >= 0 && v < this.V;
    }

    private boolean validVertices(Iterable<Integer> v) {
        if (v == null) return false;
        for (Integer i : v) if (i == null || !validVertex(i)) return false;
        return true;
    }
}

